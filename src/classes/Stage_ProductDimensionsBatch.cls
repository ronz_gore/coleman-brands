global class Stage_ProductDimensionsBatch implements Database.Batchable<sObject> {
    String query;
    global Stage_ProductDimensionsBatch() {
        
        /* Creating a dynamic query from the 'field set' so that query values can be modified by just 
        changing the field set values*/
        
        query='SELECT Id,';
        List<Schema.FieldSetMember> fieldSetMemberList =Schema.SObjectType.Staging_Product_Dimensions__c.fieldSets.getMap().get('All_Fields').getFields();
        
        for(Schema.FieldSetMember field : fieldSetMemberList)
        {
            query=query+field.getFieldPath()+',';             
        }
        query=query.substring(0,query.length()-1)+' FROM Staging_Product_Dimensions__c WHERE Is_Processed__c = false and Is_Error__c = false';
        System.debug(query);
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        
        try {
            List<Staging_Product_Dimensions__c> stageProductDimensionsList = new List<Staging_Product_Dimensions__c>();
            List<Product_Dimensions__c> productDimensionsList = new List<Product_Dimensions__c>();
            
            Set<String> productExtIdsSet= new Set<string>();
            //Set<String> plantExtIdsSet= new Set<String>();
            
            Map<String,Id> productMap = new Map<String,Id>();
            Map<String, String> productCodeToBaseUnitMap = new Map<String,String>();
            
            /*  Populating the sets for product and plant external Ids that can thus be
                used to create the Maps as external Ids with keys and record ids as values.
                These maps are used to populate the lookup values in the Actual 'Product Plannning' 
                records. 
            */
            
            for(sObject tempSProdDmnsn: scope) { 
                Staging_Product_Dimensions__c stageProductDimensions = (Staging_Product_Dimensions__c)tempSProdDmnsn;
                productExtIdsSet.add(stageProductDimensions.Material_Number__c);
                //plantExtIdsSet.add(stageProductDimensions.Valuation_Area__c);
            }
            
            for(Product2 product : [SELECT Id, Material_Number__c, Base_Unit_of_Measure__c FROM Product2 where Material_Number__c IN : productExtIdsSet]) {
                productMap.put(product.Material_Number__c, product.Id);
                productCodeToBaseUnitMap.put(product.Material_Number__c, product.Base_Unit_of_Measure__c ); 
            }
            
            
            /*
                Copying the staging data to the Actual object
                with the required transformations
            */
            for(sObject tempSProdDmnsn: scope) { 
                
                String errorMessage='';
                Staging_Product_Dimensions__c stageProductDimensions = (Staging_Product_Dimensions__c)tempSProdDmnsn;
                Boolean flag=true;
                Product_Dimensions__c productDimensions= new Product_Dimensions__c();
                
                if(stageProductDimensions.Staging_Product_Dimensions_External_Id__c != null && stageProductDimensions.Staging_Product_Dimensions_External_Id__c != '') {       
                   
                    productDimensions.Eqvlnt_Qty_in_Base_Unit_of_Measure__c = Decimal.valueOf(stageProductDimensions.Eqvlnt_Qty_in_Base_Unit_of_Measure__c);
                    productDimensions.Gross_Weight__c = Decimal.valueOf(stageProductDimensions.Gross_Weight__c);
                    productDimensions.Height__c = Decimal.valueOf(stageProductDimensions.Height__c);
                    productDimensions.International_Article_Number__c = stageProductDimensions.International_Article_Number__c;
                    productDimensions.International_Article_Number_Category__c = stageProductDimensions.International_Article_Number_Category__c;
                    productDimensions.Length__c= Decimal.valueOf(stageProductDimensions.Length__c);
                    productDimensions.Material_Number__c = stageProductDimensions.Material_Number__c;
                    productDimensions.Quantity_in_Alternative_Unit_of_Measure__c = Decimal.valueOf(stageProductDimensions.Quantity_in_Alternative_Unit_of_Measure__c);
                    productDimensions.Product_Dimensions_External_Id__c = stageProductDimensions.Staging_Product_Dimensions_External_Id__c;
                    productDimensions.Unit_of_dimension__c = stageProductDimensions.Unit_of_dimension__c;
                    productDimensions.Volume__c = Decimal.valueOf(stageProductDimensions.Volume__c);
                    productDimensions.Volume_Unit__c = stageProductDimensions.Volume_Unit__c;
                    productDimensions.Weight_Unit__c = stageProductDimensions.Weight_Unit__c;
                    productDimensions.Width__c = Decimal.valueOf(stageProductDimensions.Width__c);
                    
                    if(stageProductDimensions.Material_Number__c != null ) {
                        if(productMap.get(stageProductDimensions.Material_Number__c) != null) {
                            productDimensions.Material_Number__c = productMap.get(stageProductDimensions.Material_Number__c);
                             productDimensions.Alternative_Unit_of_Measure__c = productCodeToBaseUnitMap.get(stageProductDimensions.Material_Number__c);
                        }else{
                            flag=false;
                            errorMessage=errorMessage+'Incorrect sales organization code';
                        }
                    }
                    
                    
                    
                }
                else {
                    flag=false;
                    errorMessage = errorMessage+'Product Dimensions External Id not found';
                }
                    
                if(flag) {
                    stageProductDimensions.Is_Processed__c = true;
                    productDimensionsList.add(productDimensions);
                }else {
                    stageProductDimensions.Is_Error__c = true;
                    stageProductDimensions.Error_Description__c = errorMessage;
                }
                stageproductDimensionsList.add(stageProductDimensions);
                    
            }
            
            List<Database.UpsertResult> upsertResults = Database.upsert(productDimensionsList,Product_Dimensions__c.Product_Dimensions_External_Id__c);// upsert productList material_Number__c;
            List<Database.SaveResult> updateResults = Database.update(stageproductDimensionsList);//update stageProductList;
            
        }
        catch(Exception excptn) {
                  
        }
    }
    
    global void finish(Database.BatchableContext BC) {
    }
    
}
global class ProductBatch implements Database.Batchable<sObject> {
    String query;
    global ProductBatch (){
        query='select id,';
        List<Schema.FieldSetMember> fieldSetMemberList =Schema.SObjectType.Staging_Product__c.fieldSets.getMap().get('All_Fields').getFields();
        
        for(Schema.FieldSetMember field : fieldSetMemberList)
        {
            query=query+field .getFieldPath()+',';             
        }
        query=query.substring(0,query.length()-1)+' from Staging_Product__c where is_Processed__c=false';
        System.debug(query);
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        try{
            System.debug(query);
            String errorMessage='';
            Set<String> productHierarchySet=new Set<String>();
            Set<String> productTypeSet=new Set<String>();
            Map<String,Id> productHierarchyIdExtMap=new Map<String,Id>();
            Map<String,Id> productTypeIdExtMap=new Map<String,Id>();
            for(sObject sProduct : scope) { 
                
                Staging_Product__c stageProduct=(Staging_Product__c)sProduct;
                productTypeSet.add(stageProduct.Material_Type__c);
                productHierarchySet.add(stageProduct.Product_Hierarchy__c);
                productHierarchySet.add(stageProduct.Product_Hierarchy_L1__c);
                productHierarchySet.add(stageProduct.Product_Hierarchy_L2__c);
                productHierarchySet.add(stageProduct.Product_Hierarchy_L3__c);
                productHierarchySet.add(stageProduct.Product_Hierarchy_L4__c);
                productHierarchySet.add(stageProduct.Product_Hierarchy_L5__c);
                
            }
            
            for(Product_Hierarchy__c pH : [select id,Product_Hierarchy_Code__c from Product_Hierarchy__c where Product_Hierarchy_Code__c in: productHierarchySet]) {
                productHierarchyIdExtMap.put(ph.Product_Hierarchy_Code__c ,ph.id);
            }
            for(product_type__c type  : [select id,Product_Type_Code__c from product_type__c where Product_Type_Code__c in:productTypeSet]) {
                productTypeIdExtMap.put(type.Product_Type_Code__c,type.id);
            }
            List<product2> productList = new List<Product2>();
            List<Staging_Product__c> stageProductList=new List<Staging_Product__c>();
            for(sObject sProduct : scope) {
                Staging_Product__c stageProduct=(Staging_Product__c)sProduct;
                Boolean flag=true;
                Product2 product = new Product2();
                product.Base_Unit_of_Measure__c=stageProduct.Base_Unit_of_Measure__c;
                product.Capacity_Group__c=stageProduct.Capacity_Group__c;
                product.Crossdistributionchain_material_status__c=stageProduct.Cross_distribution_chain_material_status__c;
                product.CrossPlant_Material_Status__c=stageProduct.Cross_plant_Material_Status__c;
                product.Dangerous_Goods_Indicator_Profile__c=stageProduct.Dangerous_Goods_Indicator_Profile__c;
                product.Division__c=stageProduct.Division__c;
                product.Environmentally_Relevant__c=stageProduct.Environmentally_Relevant__c;
                product.External_Material_Group__c=stageProduct.External_Material_Group__c;
                product.Hazardous_material_number__c=stageProduct.Hazardous_material_number__c;
                product.Indicator_Highly_Viscous__c=stageProduct.Indicator_Highly_Viscous__c;
                product.Indicator_In_BulkLiquid__c=stageProduct.Indicator_In_Bulk_Liquid__c;
                product.International_Article_Number_EANUPC__c=stageProduct.International_Article_Number_EAN_UPC__c;
                product.Laboratorydesign_office__c=stageProduct.Laboratory_Design_Office__c;
                product.Manufacturer_Part_Number__c=stageProduct.Manufacturer_Part_Number__c;
                product.Description=stageProduct.Material_Description__c;
                product.Family=stageProduct.Material_Family_for_Planning_Hierarchy__c;
                product.Material_Group__c=stageProduct.Material_Group__c;
                if(stageProduct.material_Number__c!=null) {
                    product.Material_Number__c=stageProduct.material_Number__c;
                }
                else {
                    flag=false;
                    errorMessage=errorMessage+'Material Number should not be empty';
                }
                if(stageProduct.Material_Type__c!=null) {
                    product.Product_Type__c=productTypeIdExtMap.get(stageProduct.Material_Type__c);
                }
                product.Number_of_a_Manufacturer__c=stageProduct.Number_of_a_Manufacturer__c;
                product.Old_material_number__c=stageProduct.Old_material_number__c;
                if(stageProduct.Plant_material_status_valid_Date__c!=null && stageProduct.Plant_material_status_valid_Date__c!='') {
                    if(stageProduct.Plant_material_status_valid_Date__c.length()==8) {
                        String month=stageProduct.Plant_material_status_valid_Date__c.substring(4,6);
                        String day=stageProduct.Plant_material_status_valid_Date__c.substring(6,8);
                        String year=stageProduct.Plant_material_status_valid_Date__c.substring(0,4);
                        product.Cross_plant_Status_Valid_From_Date__c =Date.valueOf(year+'-'+month+'-'+day);
                    } else {
                        flag=false;
                        errorMessage=errorMessage+' Incorrect Cross Plant Status Valid from date';
                    }
                }
                // product.Cross_plant_Status_Valid_From_Date__c =stageProduct.Plant_material_status_valid_Date__c;
               
                    product.Product_Hierarchy_L1__c=productHierarchyIdExtMap.get(stageProduct.Product_Hierarchy_L1__c);
               
               
                    product.Product_Hierarchy_L2__c=productHierarchyIdExtMap.get(stageProduct.Product_Hierarchy_L2__c);
             
               
                    product.Product_Hierarchy_L3__c=productHierarchyIdExtMap.get(stageProduct.Product_Hierarchy_L3__c);
              
               
                    product.Product_Hierarchy_L4__c=productHierarchyIdExtMap.get(stageProduct.Product_Hierarchy_L4__c);
             
              
                    product.Product_Hierarchy_L5__c=productHierarchyIdExtMap.get(stageProduct.Product_Hierarchy_L5__c);
               
               
                    product.Product_Hierarchy__c=productHierarchyIdExtMap.get(stageProduct.Product_Hierarchy__c);
             
                product.Season_Category__c=stageProduct.Season_Category__c;
                product.Season_Year__c=stageProduct.Season_Year__c;
                product.Name=stageProduct.Material_Description__c;
                product.IsActive=true;
                
                if(flag){
                    stageProduct.Is_Processed__c=true;
                    productList.add(product);
                } else{
                    stageProduct.Error_Discription__c=errorMessage;
                }
                stageProductList.add(stageProduct);
            }
            List<Database.UpsertResult> results=Database.upsert(productList,Product2.material_Number__c);// upsert productList material_Number__c;
            List<Database.SaveResult> results1=Database.update(stageProductList);//update stageProductList;
        }catch(Exception e){
            System.debug(e.getMessage());
        }
    }
    
    global void finish(Database.BatchableContext BC){
    }
}
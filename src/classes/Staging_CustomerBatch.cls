global class Staging_CustomerBatch implements Database.Batchable<sObject> {
    String query;
    global Staging_CustomerBatch (){
        query='select id,';
        List<Schema.FieldSetMember> fieldSetMemberList =Schema.SObjectType.Staging_Customer__c.fieldSets.getMap().get('All_Fields').getFields();
        
        for(Schema.FieldSetMember field : fieldSetMemberList)
        {
            query=query+field .getFieldPath()+',';             
        }
        query=query.substring(0,query.length()-1)+' from Staging_Customer__c where is_Processed__c=false and Is_Error__c=false';
        System.debug(query);
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        try{
            Set<String> accountGroupExtIdsSet= new Set<string>();
            Set<String> customerClassificationExtIdsSet= new Set<String>();
            List<Account> customerList = new List<Account>();
            List<Staging_Customer__c> stageCustomerList= new List<Staging_Customer__c>();
            Map<String,Id> accountGroupMap = new Map<String,Id>();
            Map<String,Id> classificationMap = new Map<String,Id>();
            for(sObject sDoc : scope) { 
               Staging_Customer__c stageCustomer=(Staging_Customer__c)sDoc;
                customerClassificationExtIdsSet.add(stageCustomer.Customer_classification__c);
                accountGroupExtIdsSet.add(stageCustomer.Customer_Account_Group__c);
            }
            for(Customer_Group__c cusGroup : [select id,Customer_Group_Code__c from Customer_Group__c where Customer_Group_Code__c in:accountGroupExtIdsSet]){
            	accountGroupMap.put(cusGroup.Customer_Group_Code__c,cusGroup.Id);
            }
            for(CustomerChannel__c channel : [select id,Customer_Channel_Code__c from CustomerChannel__c where Customer_Channel_Code__c in:customerClassificationExtIdsSet ]){
            	classificationMap.put(channel.Customer_Channel_Code__c,channel.Id);
            }
             for(sObject sDoc : scope) { 
               Staging_Customer__c stageCustomer=(Staging_Customer__c)sDoc;
			   Account customer = new Account();
               Boolean flag=true;
               String errorMessage='';
                 if(stageCustomer.Customer_Number_1__c!=null){
                 	customer.Customer_Number_1__c=stageCustomer.Customer_Number_1__c;
                     customer.Name=stageCustomer.Name_1__c;
                     customer.Name_1__c=stageCustomer.Name_1__c;
                     customer.Name_2__c=stageCustomer.Name_2__c;
                     customer.BillingStreet=stageCustomer.House_number_and_street__c;
                     customer.BillingCity=stageCustomer.City__c;
                     customer.BillingState=stageCustomer.Region_State_Province_County__c;
                     customer.Fax=stageCustomer.Fax_Number__c;
                     customer.EMail_Address__c=stageCustomer.EMail_Address__c;
                     customer.Phone=stageCustomer.First_telephone_number__c;
                     customer.Description=stageCustomer.Description__c;
                      if(stageCustomer.Date_on_which_the_Record_Was_Created__c.length()==8 && stageCustomer.Date_on_which_the_Record_Was_Created__c.length()!=null) {
                        String month=stageCustomer.Date_on_which_the_Record_Was_Created__c.substring(4,6);
                        String day=stageCustomer.Date_on_which_the_Record_Was_Created__c.substring(6,8);
                        String year=stageCustomer.Date_on_which_the_Record_Was_Created__c.substring(0,4);
                        customer.Date_on_which_the_Record_Was_Created__c=Date.valueOf(year+'-'+month+'-'+day);
                    } else {
                        flag=false;
                        errorMessage=errorMessage+' \n Incorrect Date on which the Record Was Created';
                    }
                   	//customer.Customer_Group_Primary__c=stageCustomer.
                     customer.Previous_Master_Record_Number__c=stageCustomer.Previous_Master_Record_Number__c;
                     customer.Customer_Account_Group__c=accountGroupMap.get(stageCustomer.Customer_Account_Group__c);
                     customer.Customer_Classification__c=classificationMap.get(stageCustomer.Customer_classification__c);
                    // customer.d
                 }else{
                    flag=false;
                 	errorMessage=errorMessage+'Customer Number not there';
                 }
                 if(flag) {
                 	customerList.add(customer);
                     stageCustomer.Is_Processed__c=true;
                     stageCustomerList.add(stageCustomer);
                 }
                 else{
                    stageCustomer.Is_Error__c=true;
                     stageCustomer.Error_Discription__c=errorMessage;
                 	stageCustomerList.add(stageCustomer);
                 }
             }
            upsert customerList customer_Number_1__c;
            update stageCustomerList;
        }catch(Exception e){
            System.debug(e.getMessage());
        }
    }
    
    global void finish(Database.BatchableContext BC){
    }
}
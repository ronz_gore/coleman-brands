global Class InvokeSalesDocumentBatchClass{

  webService static String callBatchClass() {
        Staging_SalesDocumentBatch batchClass=new Staging_SalesDocumentBatch();
        system.scheduleBatch(batchClass, 'salesDocBatch',0);
        return 'Success';
   }
}
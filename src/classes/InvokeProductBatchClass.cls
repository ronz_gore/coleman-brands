global Class InvokeProductBatchClass {

  webService static String callBatchClass() {
        ProductBatch  batchClass=new ProductBatch();
        system.scheduleBatch(batchClass, 'productBatch',0);
        return 'Success';
   }
}
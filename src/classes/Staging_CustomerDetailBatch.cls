global class Staging_CustomerDetailBatch implements Database.Batchable<sObject> {
    String query;
    global Staging_CustomerDetailBatch (){
        query='select id,';
        List<Schema.FieldSetMember> fieldSetMemberList =Schema.SObjectType.Staging_Customer_Detail__c.fieldSets.getMap().get('All_Fields').getFields();
        
        for(Schema.FieldSetMember field : fieldSetMemberList)
        {
            query=query+field .getFieldPath()+',';             
        }
        query=query.substring(0,query.length()-1)+' from Staging_Customer_Detail__c where is_Processed__c=false and Is_Error__c=false';
        System.debug(query);
        
    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        try{
            List<Customer_Detail__c> customerDetailList = new List<Customer_Detail__c>();
            List<Staging_Customer_Detail__c> stagecustomerDetailList= new List<Staging_Customer_Detail__c>();
            
            Set<String> customerExtIdsSet= new Set<string>();
            Set<String> customerChannelExtIdsSet= new Set<String>();
            Set<String> customerGroupExtIdsSet= new Set<String>();
            Set<String> orderStatusExtIdsSet= new Set<string>();
            Set<String> primaryGroupExtIdsSet= new Set<String>();
            Set<String> plantExtIdsSet= new Set<String>();
            Set<String> distributionChannelExtIdsSet= new Set<string>();
            Set<String> salesDivisionExtIdsSet= new Set<String>();
            Set<String> salesDistrictExtIdsSet= new Set<String>();
            Set<String> profitCentreExtIdsSet= new Set<string>();
            Set<String> salesOfficeExtIdsSet= new Set<String>();
            Set<String> salesOrganizationExtIdsSet= new Set<String>();
            Set<String> paymentTermsExtIdsSet= new Set<string>();
            
            Map<String,Id> customerMap = new Map<String,Id>();
            Map<String,Id> customerChannelMap = new Map<String,Id>();
            Map<String,Id> customerGroupMap = new Map<String,Id>();
            Map<String,Id> customerOrderStatusMap = new Map<String,Id>();
            Map<String,Id> customerPrimaryGroupMap = new Map<String,Id>();
            Map<String,Id> PlantMap = new Map<String,Id>();
            Map<String,Id> salesDistributionChannelMap = new Map<String,Id>();
            Map<String,Id> salesDivisionMap = new Map<String,Id>();
            Map<String,Id> salesDistrictMap = new Map<String,Id>();
            Map<String,Id> salesProfitCentreMap = new Map<String,Id>();
            Map<String,Id> salesOfficeMap = new Map<String,Id>();
            Map<String,Id> salesOrganizationMap = new Map<String,Id>();
            Map<String,Id> customerPaymentTermsMap = new Map<String,Id>();
            
            for(sObject sDoc : scope) { 
                Staging_Customer_Detail__c stageCustomerDetail=(Staging_Customer_Detail__c)sDoc;
                customerExtIdsSet.add(stageCustomerDetail.Customer_Number_1__c);
                customerChannelExtIdsSet.add(stageCustomerDetail.Customer_Channel__c);
                customerGroupExtIdsSet.add(stageCustomerDetail.Customer_Group__c);
                orderStatusExtIdsSet.add(stageCustomerDetail.Customer_Group_3__c);
                primaryGroupExtIdsSet.add(stageCustomerDetail.Customer_Group_4__c);
                plantExtIdsSet.add(stageCustomerDetail.Delivering_Plant_Own_or_External__c);
                distributionChannelExtIdsSet.add(stageCustomerDetail.Distribution_Channel__c);
                salesDivisionExtIdsSet.add(stageCustomerDetail.Division__c);
                salesDistrictExtIdsSet.add(stageCustomerDetail.Sales_District__c);
                profitCentreExtIdsSet.add(stageCustomerDetail.Sales_Group__c);
                salesOfficeExtIdsSet.add(stageCustomerDetail.Sales_Office__c);
                salesOrganizationExtIdsSet.add(stageCustomerDetail.Sales_Organization__c);
                paymentTermsExtIdsSet.add(stageCustomerDetail.Terms_of_Payment_Key__c);
                
            }
            for(Account customer : [select id, customer_Number_1__c from Account where customer_Number_1__c in:customerExtIdsSet]){
                customerMap.put(customer.customer_Number_1__c,customer.Id);
            }
            for(CustomerChannel__c channel : [select id, Customer_Channel_Code__c from CustomerChannel__c where Customer_Channel_Code__c in:customerChannelExtIdsSet]){
                customerChannelMap.put(channel.Customer_Channel_Code__c,channel.Id);
            }
            for(Customer_Group__c customerGroup : [select id, Customer_Group_Code__c from Customer_Group__c where Customer_Group_Code__c in:customerGroupExtIdsSet]){
                customerGroupMap.put(customerGroup.Customer_Group_Code__c,customerGroup.Id);
            }
            for(Customer_Order_Status__c orderStatus : [select id, Customer_Order_Status_Code__c from Customer_Order_Status__c where Customer_Order_Status_Code__c in:orderStatusExtIdsSet]){
                customerOrderStatusMap.put(orderStatus.Customer_Order_Status_Code__c,orderStatus.Id);
            }
            for(Customer_Primary_Group__c primaryGroup : [select id,Customer_Primary_Group_Code__c from Customer_Primary_Group__c where Customer_Primary_Group_Code__c in:primaryGroupExtIdsSet]){
                customerPrimaryGroupMap.put(primaryGroup.Customer_Primary_Group_Code__c,primaryGroup.Id);
            }
            for(Sales_Distribution_Channel__c distributionChannel : [select id,Sales_Distribution_Channel_Code__c from Sales_Distribution_Channel__c where Sales_Distribution_Channel_Code__c in:distributionChannelExtIdsSet ]){
                salesDistributionChannelMap.put(distributionChannel.Sales_Distribution_Channel_Code__c,distributionChannel.Id);
            }
            for(Plant__c plant : [select id,Plant_Code__c from Plant__c where Plant_Code__c in:plantExtIdsSet ]){
                plantMap.put(plant.plant_Code__c,plant.id);
            }
            for(Sales_Division__c division : [select id,Sales_Division_Code__c from Sales_Division__c where Sales_Division_Code__c in:salesDivisionExtIdsSet ]){
                salesDivisionMap.put(division.Sales_Division_Code__c,division.id);
            }
            for(Sales_District__c salesDistrict : [select id,Sales_District_Code__c from Sales_District__c where Sales_District_Code__c in:salesDistrictExtIdsSet ]){
                salesDistrictMap.put(salesDistrict.Sales_District_Code__c,salesDistrict.id);
            }
            for(Sales_Profit_Centre__c profitcentre : [select id,Sales_Profit_Centre_Code__c from Sales_Profit_Centre__c where Sales_Profit_Centre_Code__c in:profitCentreExtIdsSet ]){
                salesProfitCentreMap.put(profitcentre.Sales_Profit_Centre_Code__c,profitcentre.id);
            }
            for(Sales_Office__c salesOffice : [select id,Sales_Office_Code__c from Sales_Office__c where Sales_Office_Code__c in:salesOfficeExtIdsSet ]){
                salesOfficeMap.put(salesOffice.Sales_Office_Code__c,salesOffice.id);
            }
            for(Sales_Organization__c salesOrg : [select id,Sales_Organization_Code__c from Sales_Organization__c where Sales_Organization_Code__c in:salesOrganizationExtIdsSet ]){
                salesOrganizationMap.put(salesOrg.Sales_Organization_Code__c,salesOrg.id);
            }
            for(Customer_Payment_Terms__c paymentTerms : [select id,Customer_Payment_Terms_Code__c from Customer_Payment_Terms__c where Customer_Payment_Terms_Code__c in:paymentTermsExtIdsSet ]){
                customerPaymentTermsMap.put(paymentTerms.Customer_Payment_Terms_Code__c,paymentTerms.id);
            }
            for(sObject sDoc : scope) { 
                Staging_Customer_Detail__c stageCustomerDetail=(Staging_Customer_Detail__c)sDoc;
                Customer_Detail__c customerDetail = new Customer_Detail__c();
                Boolean flag=true;
                String errorMessage='';
                if(stageCustomerDetail.Staging_Customer_Detail_External_Id__c!=null &&stageCustomerDetail.Staging_Customer_Detail_External_Id__c!=''){
                    customerDetail.Customer_Detail_External_Id__c=stageCustomerDetail.Staging_Customer_Detail_External_Id__c;
                    customerDetail.Currency__c=stageCustomerDetail.Currency__c;
                    if(stageCustomerDetail.Customer_Number_1__c!=null){
                        if(customerMap.get(stageCustomerDetail.Customer_Number_1__c)!=null) {
                            customerDetail.Customer__c=customerMap.get(stageCustomerDetail.Customer_Number_1__c);
                        }else{
                            flag=false;
                             errorMessage=errorMessage+'Incorrect customer Number';
                        }
                    }
                    customerDetail.Customer_Account_Code_at_Customer__c=stageCustomerDetail.Customer_Account_Code_at_Customer__c;
                    if(stageCustomerDetail.Customer_Channel__c!=null){
                        if(customerChannelMap.get(stageCustomerDetail.Customer_Channel__c)!=null) {
                            customerDetail.Customer_Channel__c=customerChannelMap.get(stageCustomerDetail.Customer_Channel__c);
                        }else{
                            flag=false;
                             errorMessage=errorMessage+'Incorrect customer channel';
                        }
                    }
                    customerDetail.Customer_Classification_ABC_Analysis__c=stageCustomerDetail.Customer_Classification_ABC_Analysis__c;
                    customerDetail.Customer_Number_1__c=stageCustomerDetail.Customer_Number_1__c;
                    customerDetail.Customer_Statistics_Group__c=stageCustomerDetail.Customer_Statistics_Group__c;
                    customerDetail.Incoterms_Part_1__c=stageCustomerDetail.Incoterms_Part_1__c;
                    customerDetail.Incoterms_Part_2__c=stageCustomerDetail.Incoterms_Part_2__c;
                    customerDetail.Invoice_Dates_Calendar_Identification__c=stageCustomerDetail.Invoice_Dates_Calendar_Identification__c;
                    customerDetail.Invoice_List_Schedule__c=stageCustomerDetail.Invoice_List_Schedule__c;
                    customerDetail.Price_Group_Customer__c=stageCustomerDetail.Price_Group_Customer__c;
                    customerDetail.Price_List_Type__c=stageCustomerDetail.Price_List_Type__c;
                    customerDetail.Pricing_Procedure__c=stageCustomerDetail.Pricing_Procedure__c;
                     if(stageCustomerDetail.Customer_Group__c!=null){
                        if(customerGroupMap.get(stageCustomerDetail.Customer_Group__c)!=null) {
                            customerDetail.Customer_Group__c=customerGroupMap.get(stageCustomerDetail.Customer_Group__c);
                        }else{
                            flag=false;
                             errorMessage=errorMessage+'Incorrect customer group code';
                        }
                    }
                     if(stageCustomerDetail.Customer_Group_3__c!=null){
                        if(customerOrderStatusMap.get(stageCustomerDetail.Customer_Group_3__c)!=null) {
                            customerDetail.Customer_Group_3__c=customerOrderStatusMap.get(stageCustomerDetail.Customer_Group_3__c);
                        }else{
                            flag=false;
                             errorMessage=errorMessage+'Incorrect customer group 3 code';
                        }
                    }
                     if(stageCustomerDetail.Customer_Group_4__c!=null){
                        if(customerPrimaryGroupMap.get(stageCustomerDetail.Customer_Group_4__c)!=null) {
                            customerDetail.Customer_Group_4__c=customerPrimaryGroupMap.get(stageCustomerDetail.Customer_Group_4__c);
                        }else{
                            flag=false;
                             errorMessage=errorMessage+'Incorrect customer group 4 code';
                        }
                    }
                    if(stageCustomerDetail.Distribution_Channel__c!=null){
                        if(salesDistributionChannelMap.get(stageCustomerDetail.Distribution_Channel__c)!=null) {
                            customerDetail.Distribution_Channel__c=salesDistributionChannelMap.get(stageCustomerDetail.Distribution_Channel__c);
                        }else{
                            flag=false;
                             errorMessage=errorMessage+'Incorrect distribution channel';
                        }
                    }
                     if(stageCustomerDetail.Delivering_Plant_Own_or_External__c!=null){
                        if(plantMap.get(stageCustomerDetail.Delivering_Plant_Own_or_External__c)!=null) {
                            customerDetail.Delivering_Plant_Own_or_External__c=plantMap.get(stageCustomerDetail.Delivering_Plant_Own_or_External__c);
                        }else{
                            flag=false;
                             errorMessage=errorMessage+'Incorrect plant code';
                        }
                    }
                     if(stageCustomerDetail.Division__c!=null){
                        if(salesDivisionMap.get(stageCustomerDetail.Division__c)!=null) {
                            customerDetail.Division__c=salesDivisionMap.get(stageCustomerDetail.Division__c);
                        }else{
                            flag=false;
                             errorMessage=errorMessage+'Incorrect division code';
                        }
                    }
                     if(stageCustomerDetail.Sales_District__c!=null){
                        if(salesDistrictMap.get(stageCustomerDetail.Sales_District__c)!=null) {
                            customerDetail.Sales_District__c=salesDistrictMap.get(stageCustomerDetail.Sales_District__c);
                        }else{
                            flag=false;
                             errorMessage=errorMessage+'Incorrect sales district code';
                        }
                    }
                    if(stageCustomerDetail.Sales_Group__c!=null){
                        if(salesProfitCentreMap.get(stageCustomerDetail.Sales_Group__c)!=null) {
                            customerDetail.Sales_Group__c=salesProfitCentreMap.get(stageCustomerDetail.Sales_Group__c);
                        }else{
                            flag=false;
                            errorMessage=errorMessage+'Incorrect sales profit centre code';
                        }
                    }
                    if(stageCustomerDetail.sales_office__c!=null){
                        if(salesOfficeMap.get(stageCustomerDetail.sales_office__c)!=null) {
                            customerDetail.sales_office__c=salesOfficeMap.get(stageCustomerDetail.sales_office__c);
                        }else{
                            flag=false;
                            errorMessage=errorMessage+'Incorrect sales office code';
                        }
                    }
                    if(stageCustomerDetail.Sales_Organization__c!=null){
                        if(salesOrganizationMap.get(stageCustomerDetail.Sales_Organization__c)!=null) {
                            customerDetail.Sales_Organization__c=salesOrganizationMap.get(stageCustomerDetail.Sales_Organization__c);
                        }else{
                            flag=false;
                             errorMessage=errorMessage+'Incorrect sales organization code';
                        }
                    }
                     if(stageCustomerDetail.Terms_of_Payment_Key__c!=null){
                        if(customerPaymentTermsMap.get(stageCustomerDetail.Terms_of_Payment_Key__c)!=null) {
                            customerDetail.Terms_of_Payment_Key__c=customerPaymentTermsMap.get(stageCustomerDetail.Terms_of_Payment_Key__c);
                        }else{
                            flag=false;
                             errorMessage=errorMessage+'Incorrect terms of payment key';
                        }
                    }
                }
                else{
                    flag=false;
                    errorMessage=errorMessage+'Customer Detail external Id not found';
                }
                 if(flag) {
                    customerDetailList.add(customerDetail);
                     stageCustomerDetail.Is_Processed__c=true;
                     stagecustomerDetailList.add(stageCustomerDetail);
                 }
                 else{
                    stageCustomerDetail.Is_Error__c=true;
                     stageCustomerDetail.Error_Discription__c=errorMessage;
                    stagecustomerDetailList.add(stageCustomerDetail);
                 }
            }
            upsert customerDetailList Customer_Detail_External_Id__c;
            update stagecustomerDetailList;
            
        }catch(Exception e){
            System.debug(e.getMessage());
        }
    }
    
    global void finish(Database.BatchableContext BC){
    }
}
/*******
Developer Name: Anjith kumar
Class Name: CreateCheckListController
Version: 1.0
Description: This class used for visualise,create,edit,delete checklist,assign checklist and cancel assignment to acccounts(Stores)
Usage : This class for visualize the CreateChecklist vf Page
*******/
public class CreateCheckListController {

    public PageReference goToHome() {
        return new PageReference(URL.getSalesforceBaseUrl().toExternalForm());
    }

    public List<Survey__c> checkList{get; set;}
    public Survey__c newChecklist { get{return new Survey__c();} set;}
    public String homeUrl{get;set;}
    public CreateCheckListController (){
        checkList=new List<Survey__c>();
        checkList=[select id, name from survey__c];
        homeUrl=URL.getSalesforceBaseUrl().toExternalForm();
    }
    
    @RemoteAction
        public static List<Survey__c> getCheckList(){
            
            return [select id,name from Survey__c];
        }
    
    @RemoteAction
        public static Id createChecklistAndQuestions(String checklistName,String question,String type,String choice,String required,String orderNumber){
            Survey__c checklist;
            
            try{
                checklist=[select id from survey__C where name=:checklistName];
                
            }
            catch(Exception e){
                checklist=new Survey__c(Name=checklistName,Submit_Response__c='test',Survey_Container_CSS__c='#survey_container{ margin: 0 auto; width: 600px; box-shadow: 0 0 14px #CCCCCC; -moz-box-shadow: 0 0 14px #CCCCCC; -webkit-box-shadow: 0 0 14px #CCCCCC; }');
            }
            String questionType;
            if(type=='singleSelectV') {
                questionType='Single Select';
            }else if(type=='multiSelect') {
                questionType='Multi Select';
            }else{
                questionType='Free Text';
            }
            upsert checklist;
            List<Survey_Question__c> questionsList=new List<Survey_Question__c>();
            questionsList.add(new Survey_Question__c(Type__C=questionType,Survey__c=checklist.id,Name=question,Question__c=question,Choices__c=choice,Required__c=Boolean.valueOf(required),OrderNumber__c=Integer.valueOf(orderNumber)));
            database.insert(questionsList);
            return checklist.id;
        }
    @RemoteAction
        public static List<Survey_Question__c> getQuestions(String checklistId){
            return [select id,Question__c,Choices__c,Required__c,OrderNumber__c,Type__C from Survey_Question__c where survey__c=:checklistId];
        }
    @RemoteAction
        public static List<Account> searchAccounts(String accName,List<String> accountSet){
            String query='select id,Name,BillingCity,BillingCountry from account where Name like \'%'+accName+'%\'';
            for(String accountId: accountSet){
                query=query+' and id!=\''+accountId+'\'';
            }
            return Database.query(query);
            
        }
    @RemoteAction
        public static List<Account> getChecklistAssignAccounts(String checklistId){
            Set<Id> accountSet=new Set<Id>();
            for(SurveyTaker__c assignment: [select id,Account__c from SurveyTaker__c where Survey__c=:checklistId and is_Active__c=true]) {
                accountSet.add(assignment.Account__c);
                
            }
            return [select id,Name,BillingCity,BillingCountry from Account where id in: accountSet order by name];
            
        }
    @RemoteAction
        public static String createChecklistAssignments(String checklistId,List<String> accountIds){
            List<SurveyTaker__c> checklistAssignmentList=new List<SurveyTaker__c>();
            set<String> childAndParentAccIds=new set<string>();
            try{
                
                childAndParentAccIds =new Map<String,Account>([select id,name from account where parentId in:accountIds]).KeySet();
                accountIds.addAll(childAndParentAccIds );
            }catch(Exception e){
                //childAndParentAccIds=new set<string>();
            }
            //for(Integer i=0;i<accountIds.size();i++){
            List<FeedItem> feedItemsList=new List<FeedItem>();
            for(String accId : accountIds){
                checklistAssignmentList.add(new SurveyTaker__c(Account__c=accId ,Is_Active__c=true,Survey__c=checklistId));
                //Adding a Link post
                FeedItem post = new FeedItem();
                post.ParentId = accId; //eg. Opportunity id, custom object id..
                post.Body = 'A checklist is assigned to this account';
                post.Title='Launch Checklist';
                post.LinkUrl = '/apex/launchchecklistv2?Id='+accId;
                feedItemsList.add(post);
            }
           
            List<SurveyTaker__c> checklistAssignList= new List<SurveyTaker__c>();
            for(SurveyTaker__c checklistAssign: [select id,Is_Active__c from SurveyTaker__c where Account__c in:accountIds and (Is_Active__c=true OR Is_Submitted__c=true)]){
            checklistAssign.Is_Active__c=false;
            checklistAssign.Is_Submitted__c=false;
            checklistAssignList.add(checklistAssign);
            }
            try{
             Database.Insert(checklistAssignmentList);
            Database.Insert(feedItemsList);
            Database.update(checklistAssignList);
            }catch(Exception e){}
            return 'Success';
        }
    @RemoteAction
        public static String cancelAssignment(String checklistId,String accountId){
            try{
                List<SurveyTaker__c>  cAssignment= new List<SurveyTaker__c>();
                for(SurveyTaker__c taker : [select id,is_Active__c from SurveyTaker__c where account__c=:accountId and survey__c=:checklistId]) {
                    taker .is_Active__c=false;
                    cAssignment.add(taker);
                }
                update cAssignment;
            }catch(Exception e){
                
            }
            return 'Success';
            
        }
    @RemoteAction
        public static String deleteChecklist(String checklistId){
            delete new Survey__c(Id=checklistId);
            return 'Success';
        }
    @RemoteAction
        public static String updateCheckList(String checklistId,String name){
            try{
                survey__c checklist= [select id,name from survey__c where id=:checklistId];
                checklist.name=name;
                update checklist;
            }
            catch(Exception e){
                System.debug(e.getMessage());
            }
            return 'Success';
        }
    @RemoteAction
        public static String updateQuestion(String questionId,String question,String choices,String type,String req){
            Boolean required=Boolean.valueOf(req);
            update new Survey_Question__c(id=questionId,Question__c=question,Choices__c=choices,Required__c=required,Type__c=type);
            return 'Success';
            
        }
    @RemoteAction
        public static String deleteQuestion(String questionId){
            
            delete new Survey_Question__c(id=questionId);
            return 'Success';
        }
    @RemoteAction
        public static List<Survey__c> searchChecklist(String checklistName){
            String query='select id,name from survey__c where Name like \'%'+checklistName+'%\'';
            return database.query(query);
            
        }
    @RemoteAction
        public static Survey_Question__c addQuestion(String checklistId,String question,String type,String choices,String required){
            integer orderNumber=1;
            try{
                orderNumber=Integer.valueOf([select id,orderNumber__c from Survey_Question__c where survey__c=:checklistId order by orderNumber__c desc limit 1].orderNumber__c +1);
            }catch(Exception e){}
            String questionType;
            if(type=='singleSelectV') {
                questionType='Single Select';
            }else if(type=='multiSelect') {
                questionType='Multi Select';
            }else{
                questionType='Free Text';
            }
            Survey_Question__c checklistQuestion=new Survey_Question__c(Survey__c=checklistId,Name=question,Question__c=question,Choices__c=choices,Required__c=Boolean.valueOf(required),Type__c=questionType,orderNumber__c=orderNumber);
            insert checklistQuestion;
            return checklistQuestion;
        }
    
}
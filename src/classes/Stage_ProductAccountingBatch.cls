global class Stage_ProductAccountingBatch implements Database.Batchable<sObject> {
    String query;
    global Stage_ProductAccountingBatch() {
        
        /* Creating a dynamic query from the 'field set' so that query values can be modified by just 
        changing the field set values*/
        
        query='SELECT Id,';
        List<Schema.FieldSetMember> fieldSetMemberList =Schema.SObjectType.Staging_Product_Accounting__c.fieldSets.getMap().get('All_Fields').getFields();
        
        for(Schema.FieldSetMember field : fieldSetMemberList)
        {
            query=query+field.getFieldPath()+',';             
        }
        query=query.substring(0,query.length()-1)+' FROM Staging_Product_Accounting__c WHERE Is_Processed__c = false and Is_Error__c = false';
        System.debug(query);
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        
        try {
            List<Staging_Product_Accounting__c> stageProductAccountingList = new List<Staging_Product_Accounting__c>();
            List<Product_Accounting__c> productAccountingList = new List<Product_Accounting__c>();
            
            Set<String> productExtIdsSet= new Set<string>();
            Set<String> plantExtIdsSet= new Set<String>();
            
            Map<String,Id> productMap = new Map<String,Id>();
            Map<String,Id> plantMap = new Map<String,Id>();
            
            /*  Populating the sets for product and plant external Ids that can thus be
                used to create the Maps as external Ids with keys and record ids as values.
                These maps are used to populate the lookup values in the Actual 'Product Plannning' 
                records. 
            */
            
            for(sObject tempSProdAccnt: scope) { 
                Staging_Product_Accounting__c stageProductAccounting = (Staging_Product_Accounting__c)tempSProdAccnt;
                productExtIdsSet.add(stageProductAccounting.Material_Number__c);
                plantExtIdsSet.add(stageProductAccounting.Valuation_Area__c);
            }
            
            for(Product2 product : [SELECT Id, Material_Number__c FROM Product2 where Material_Number__c IN : productExtIdsSet]) {
                productMap.put(product.Material_Number__c, product.Id);
            }
            for(Plant__c plant : [select id, Plant_Code__c from Plant__c where Plant_Code__c IN : plantExtIdsSet]){
                plantMap.put(plant.Plant_Code__c , plant.Id);
            }
            
            /*
                Copying the staging data to the Actual object
                with the required transformations
            */
            for(sObject tempSProdAccnt: scope) { 
                
                String errorMessage='';
                Staging_Product_Accounting__c stageProductAccounting = (Staging_Product_Accounting__c )tempSProdAccnt;
                Boolean flag=true;
                Product_Accounting__c productAccounting= new Product_Accounting__c();
                
                if(stageProductAccounting.Staging_Product_Accounting_External_Id__c != null &&stageProductAccounting.Staging_Product_Accounting_External_Id__c != '') {       
                   
                    productAccounting.Costing_Overhead_Group__c = stageProductAccounting.Costing_Overhead_Group__c; 
                    productAccounting.Future_Price__c = Decimal.valueOf(stageProductAccounting.Future_Price__c);
                    productAccounting.Future_Price_Valid_From_Date__c = Decimal.valueOf(stageProductAccounting.Future_Price_Valid_From_Date__c );
                    productAccounting.Moving_Price__c = Decimal.valueOf(stageProductAccounting.Moving_Price__c);
                    productAccounting.Moving_Average_Price_in_Previous_Year__c = Decimal.valueOf(stageProductAccounting.Moving_Average_Price_in_Previous_Year__c);
                    productAccounting.Origin_Group__c = stageProductAccounting.Origin_Group__c; 
                    productAccounting.Price_Control_Indicator__c = stageProductAccounting.Price_Control_Indicator__c;
                    productAccounting.Price_Unit__c = Decimal.valueOf(stageProductAccounting.Price_Unit__c);
                    productAccounting.Product_Accounting_External_Id__c = stageProductAccounting.Staging_Product_Accounting_External_Id__c;
                    productAccounting.Standard_Price__c = Decimal.valueOf(stageProductAccounting.Standard_Price__c);
                    productAccounting.Standard_Price_in_Previous_Year__c = Decimal.valueOf(stageProductAccounting.Standard_Price_in_Previous_Year__c);
                    productAccounting.Total_Valuated_Stock__c = Decimal.valueOf(stageProductAccounting.Total_Valuated_Stock__c);
                    productAccounting.Valuation_Class__c = stageProductAccounting.Valuation_Class__c;
                    productAccounting.Valuation_Class_in_Previous_Year__c= stageProductAccounting.Valuation_Class_in_Previous_Year__c;
                    productAccounting.Value_of_Total_Valuated_Stock__c = Decimal.valueOf(stageProductAccounting.Value_of_Total_Valuated_Stock__c);
                   
                    if(stageProductAccounting.Material_Number__c != null ) {
                        if(productMap.get(stageProductAccounting.Material_Number__c) != null) {
                            productAccounting.Material_Number__c = productMap.get(stageProductAccounting.Material_Number__c);
                        }else{
                            flag=false;
                            errorMessage=errorMessage+'Incorrect sales organization code';
                        }
                    }
                    
                    if(stageProductAccounting.Valuation_Area__c != null) {
                        productAccounting.Valuation_Area__c = plantMap.get(stageProductAccounting.Valuation_Area__c); 
                    }
                    
                }
                else {
                    flag=false;
                    errorMessage = errorMessage+'Product Accounting External Id not found';
                }
                    
                if(flag) {
                    stageProductAccounting.Is_Processed__c = true;
                    productAccountingList.add(productAccounting);
                }else {
                    stageProductAccounting.Is_Error__c = true;
                    stageProductAccounting.Error_Description__c = errorMessage;
                }
                stageProductAccountingList.add(stageProductAccounting);
                    
            }
            
            List<Database.UpsertResult> upsertResults = Database.upsert(productAccountingList,Product_Accounting__c.Product_Accounting_External_Id__c);// upsert productList material_Number__c;
            List<Database.SaveResult> updateResults = Database.update(stageProductAccountingList);//update stageProductList;
            
        }
        catch(Exception excptn) {
                  
        }
    }
    
    global void finish(Database.BatchableContext BC) {
    }
    
}
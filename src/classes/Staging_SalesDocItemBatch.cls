global class Staging_SalesDocItemBatch implements Database.Batchable<sObject> {
    String query;
    global Staging_SalesDocItemBatch (){
        query='select id,';
        List<Schema.FieldSetMember> fieldSetMemberList =Schema.SObjectType.Staging_Sales_Document_Item__c.fieldSets.getMap().get('All_Fields').getFields();
        
        for(Schema.FieldSetMember field : fieldSetMemberList)
        {
            query=query+field .getFieldPath()+',';             
        }
        query=query.substring(0,query.length()-1)+' from Staging_Sales_Document_Item__c where is_Processed__c=false and Is_Error__c=false';
        System.debug(query);
        
    }
 global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        try{
            Set<String> productExtIdsSet= new Set<string>();
            Set<String> salesDocExtIdsSet= new Set<String>();
            Set<String> plantExtIdsSet= new Set<String>();
            List<Sales_Document_Item__c> salesDocItemList = new List<Sales_Document_Item__c>();
            List<Staging_Sales_Document_Item__c> stageSalesDocItemList= new List<Staging_Sales_Document_Item__c>();
            Map<String,Id> productMap = new Map<String,Id>();
            Map<String,Id> plantMap = new Map<String,Id>();
            Map<String,Id> salesDocMap = new Map<String,Id>();
            for(sObject sDoc : scope) { 
                Staging_Sales_Document_Item__c stageSalesDocItem=(Staging_Sales_Document_Item__c)sDoc;
                productExtIdsSet.add(stageSalesDocItem.Material_Number__c);
                salesDocExtIdsSet.add(stageSalesDocItem.Sales_Document__c);
                plantExtIdsSet.add(stageSalesDocItem.Plant__c);
            }
            for(Product2 prod : [select id,Material_Number__c from Product2 where Material_Number__c in:productExtIdsSet]){
            	productMap.put(prod.Material_Number__c,prod.Id);
            }
            for(Opportunity opp : [select id,Sales_Document__c from Opportunity where Sales_Document__c in:SalesDocExtIdsSet ]){
            	salesDocMap.put(opp.Sales_Document__c,opp.Id);
            }
            for(Plant__c plant : [select id,Plant_Code__c from Plant__c where Plant_Code__c in:plantExtIdsSet ]){
                	plantMap.put(plant.plant_Code__c,plant.id);
            }
             for(sObject sDoc : scope) { 
               Staging_Sales_Document_Item__c stageSalesDocItem=(Staging_Sales_Document_Item__c)sDoc;
			   Sales_Document_Item__c salesDocItem = new Sales_Document_Item__c();
               Boolean flag=true;
               String errorMessage='';
                 if(stageSalesDocItem.Sales_Document_Item_Code__c!=null){
                    	salesDocItem.Sales_Document_Item_Code__c=stageSalesDocItem.Sales_Document_Item_Code__c;
                     if(stageSalesDocItem.Material_Number__c!=null){
                      	 if(productMap.get(stageSalesDocItem.Material_Number__c)!=null)
                    		salesDocItem.Material_Number__c=productMap.get(stageSalesDocItem.Material_Number__c);
                         else{
                                flag=false;
                        		errorMessage=errorMessage+' \n Incorrect Material Number';
                         }
                         	
                      }
                    
                    salesDocItem.Cumulative_Order_Quantity__c=stageSalesDocItem.Cumulative_Order_Quantity__c;
                    salesDocItem.Fiscal_Year__c=stageSalesDocItem.Fiscal_Year__c;
                    salesDocItem.Period__c=stageSalesDocItem.Period__c;
                     
                     if(stageSalesDocItem.Sales_Document__c!=null){
                         if(salesDocMap.get(stageSalesDocItem.Sales_Document__c)!=null)
                    		salesDocItem.Sales_Document__c=salesDocMap.get(stageSalesDocItem.Sales_Document__c);
                         else
                         {
                                flag=false;
                        		errorMessage=errorMessage+' \n Incorrect Sales Document Code';
                         }
                     }
                     if(stageSalesDocItem.Plant__c!=null){
                      	 if(plantMap.get(stageSalesDocItem.Plant__c)!=null)
                    		salesDocItem.Plant__c=plantMap.get(stageSalesDocItem.Plant__c);
                         else{
                                flag=false;
                        		errorMessage=errorMessage+' \n Incorrect Plant Code';
                         }
                         	
                      }
                    
                    if(stageSalesDocItem.Posting_Date__c.length()==8 && stageSalesDocItem.Posting_Date__c.length()!=null) {
                        String month=stageSalesDocItem.Posting_Date__c.substring(4,6);
                        String day=stageSalesDocItem.Posting_Date__c.substring(6,8);
                        String year=stageSalesDocItem.Posting_Date__c.substring(0,4);
                        salesDocItem.Posting_Date__c=Date.valueOf(year+'-'+month+'-'+day);
                    } else {
                        flag=false;
                        errorMessage=errorMessage+' \n Incorrect Posting Date';
                    }
                   	
                 }else{
                    flag=false;
                 	errorMessage=errorMessage+'Sales Document Item code is not there or Incorrect';
                 }
                 if(flag) {
                 	salesDocItemList.add(salesDocItem);
                     stageSalesDocItem.Is_Processed__c=true;
                     stageSalesDocItemList.add(stageSalesDocItem);
                 }
                 else{
                    stageSalesDocItem.Is_Error__c=true;
                     stageSalesDocItem.Error_Discription__c=errorMessage;
                 	stageSalesDocItemList.add(stageSalesDocItem);
                 }
             }
            upsert salesDocItemList Sales_Document_Item_Code__c;
            update stageSalesDocItemList;
        }catch(Exception e){
            System.debug(e.getMessage());
        }
    }
    
    global void finish(Database.BatchableContext BC){
    }
}
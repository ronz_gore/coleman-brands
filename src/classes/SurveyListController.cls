public with sharing class SurveyListController {

    public SurveyListController(ApexPages.StandardController controller) {

    }


Public List<Survey__c> surveyList {get; set;}
Set<Id> surveyIds;
Public SurveyListController(){
    
    string queryString = 'SELECT Id FROM Survey__c';
    
    surveyList = Database.query(queryString);

}
public pageReference validSurvey(){
    Boolean isActive = FALSE;
    Id surveyId;
    PageReference reference = NULL;
    Id id = apexpages.currentpage().getparameters().get('id');
    surveyIds = new Set<Id>();
    List<SurveyTaker__c> surveyTakenList  = [SELECT Survey__c, Is_Active__c FROM SurveyTaker__c WHERE Account__c = :id Order by Is_Active__c];
    for(SurveyTaker__c ST: surveyTakenList){
        if(ST.Is_Active__c){
            isActive = TRUE;
            surveyId = ST.Survey__c;
            break;
        }
        surveyIds.add(ST.Survey__c);
    }
    
    if(isActive){
        reference = page.SurveyManagerPage;
        reference.setRedirect(true);
        reference.getParameters().put('id',surveyId);
    }
return reference;
}
}
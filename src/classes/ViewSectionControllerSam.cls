public class ViewSectionControllerSam {
    
    public List<QuestionAnswer> questionAnswerList{get;set;}
    
    public ViewSectionControllerSam(ApexPages.StandardController controller) {
        //SurveyTaker__c s = (SurveyTaker__c)controller.getrecord();
        Id sId = ApexPages.currentPage().getParameters().get('id');
        subSection(sId);
        //System.assertEquals(sId+'',null);
    }
    
   
    public void subSection(Id checkListId) {
        list<SurveyTaker__c> checklistAssignmentList = [select Id, (select Id, Response__c, Survey_Question__c, SurveyTaker__c from Checklist_Question_Answers__r) 
                                                               from SurveyTaker__c WHERE Id =: checkListId limit 1];
         //System.assertEquals(checklistAssignmentList +'',null);                                                       
        if(checklistAssignmentList.size()>0){
             list<Id> surveyQuestionsId = new list<Id>(); 
             for(SurveyQuestionResponse__c tempResponse : checklistAssignmentList[0].Checklist_Question_Answers__r){
                 surveyQuestionsId.add(tempResponse.Survey_Question__c);    
             }
             Map<Id,Survey_Question__c > idsAndQuestions = new Map<Id, Survey_Question__c > ([select Id, Question__c from Survey_Question__c where Id IN : surveyQuestionsId]);      
            questionAnswerList = new List<QuestionAnswer>();
            for(SurveyQuestionResponse__c tempResponse : checklistAssignmentList[0].Checklist_Question_Answers__r) {
                QuestionAnswer quesAns = new QuestionAnswer(idsAndQuestions.get(tempResponse.Survey_Question__c).Question__c, tempResponse.Response__c);
                questionAnswerList.add(quesAns);                                                       
            }     
        }                                                         
               
                                                          
                                           
    }
    
    public class QuestionAnswer{
        
        public QuestionAnswer(String question,String response) {
            this.question = question;
            this.response = response;
        }
        public String question {get;set;}
        public String response {get;set;}
    }
}
global class Stage_ProductPlanningBatch implements Database.Batchable<sObject> {
    String query;
    global Stage_ProductPlanningBatch() {
        
        /* Creating a dynamic query from the 'field set' so that query values can be modified by just 
        changing the field set values*/
        
        query='SELECT Id,';
        List<Schema.FieldSetMember> fieldSetMemberList =Schema.SObjectType.Stage_Product_Planning__c.fieldSets.getMap().get('All_Fields').getFields();
        
        for(Schema.FieldSetMember field : fieldSetMemberList)
        {
            query=query+field.getFieldPath()+',';             
        }
        query=query.substring(0,query.length()-1)+' FROM Stage_Product_Planning__c WHERE is_Processed__c=false and Is_Error__c=false';
        System.debug(query);
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        
        try {
            List<Stage_Product_Planning__c> stageProductPlanningList = new List<Stage_Product_Planning__c>();
            List<Product_Planning__c> productPlanningList = new List<Product_Planning__c>();
            
            Set<String> productExtIdsSet= new Set<string>();
            Set<String> plantExtIdsSet= new Set<String>();
            
            Map<String,Id> productMap = new Map<String,Id>();
            Map<String,Id> plantMap = new Map<String,Id>();
            
            /*  Populating the sets for product and plant external Ids that can thus be
                used to create the Maps as external Ids with keys and record ids as values.
                These maps are used to populate the lookup values in the Actual 'Product Plannning' 
                records. 
            */
            
            for(sObject tempSProdPlan : scope) { 
                Stage_Product_Planning__c stageProductPlanning = (Stage_Product_Planning__c)tempSProdPlan;
                productExtIdsSet.add(stageProductPlanning.Material_Number__c);
                plantExtIdsSet.add(stageProductPlanning.Plant__c);
            }
            
            for(Product2 product : [SELECT Id, Material_Number__c FROM Product2 where Material_Number__c IN : productExtIdsSet]) {
                productMap.put(product.Material_Number__c, product.Id);
            }
            for(Plant__c plant : [select id, Plant_Code__c from Plant__c where Plant_Code__c IN : plantExtIdsSet]){
                plantMap.put(plant.Plant_Code__c , plant.Id);
            }
            
            /*
                Copying the staging data to the Actual object
                with the required transformations
            */
            for(sObject tempSProdPlan : scope) { 
                
                String errorMessage='';
                Stage_Product_Planning__c stageProductPlanning = (Stage_Product_Planning__c )tempSProdPlan;
                Boolean flag=true;
                Product_Planning__c productPlanning = new Product_Planning__c();
                
                if(stageProductPlanning.Staging_Product_Planning_External_Id__c!= null &&stageProductPlanning.Staging_Product_Planning_External_Id__c != '') {       
                   
                    productPlanning.ABC_Indicator__c = stageProductPlanning.ABC_Indicator__c ; 
                    productPlanning.Checking_Group_for_Availability_Check__c = stageProductPlanning.Checking_Group_for_Availability_Check__c ;
                    productPlanning.Consumption_mode__c = stageProductPlanning.Consumption_mode__c ;
                    productPlanning.Consumption_period_backward__c = Decimal.valueOf(stageProductPlanning.Consumption_period_backward__c );
                    productPlanning.Fiscal_Year_Variant__c = stageProductPlanning.Fiscal_Year_Variant__c ;
                    productPlanning.Fixed_lot_size__c = Decimal.valueOf(stageProductPlanning.Fixed_lot_size__c ); 
                    productPlanning.Forecast_Group__c = stageProductPlanning.Forecast_Group__c ;
                    productPlanning.Goods_Receipt_Processing_Time_in_Days__c = Decimal.valueOf(stageProductPlanning.Goods_Receipt_Processing_Time_in_Days__c );
                    productPlanning.Indicator_Backflush__c = stageProductPlanning.Indicator_Backflush__c ;
                    productPlanning.In_house_production_time__c = Decimal.valueOf(stageProductPlanning.In_house_production_time__c );
                    productPlanning.Issue_Storage_Location__c = stageProductPlanning.Issue_Storage_Location__c ;
                    productPlanning.Lot_size_materials_planning__c = Decimal.valueOf(stageProductPlanning.Lot_size_materials_planning__c );
                    productPlanning.Maximum_Lot_Size__c = Decimal.valueOf(stageProductPlanning.Maximum_Lot_Size__c );
                    productPlanning.Maximum_stock_level__c = Decimal.valueOf(stageProductPlanning.Maximum_stock_level__c );
                    productPlanning.Minimum_Lot_Size__c  = Decimal.valueOf(stageProductPlanning.Minimum_Lot_Size__c  );
                    productPlanning.Minimum_Safety_Stock__c = Decimal.valueOf(stageProductPlanning.Minimum_Safety_Stock__c );
                    productPlanning.MRP_Type__c = stageProductPlanning.MRP_Type__c ;
                    productPlanning.Period_Indicator__c = stageProductPlanning.Period_Indicator__c ;
                    productPlanning.Planned_Delivery_Time_in_Days__c = Decimal.valueOf(stageProductPlanning.Planned_Delivery_Time_in_Days__c );
                    productPlanning.Planning_strategy_group__c = stageProductPlanning.Planning_strategy_group__c ;
                    productPlanning.PPC_planning_calendar__c = stageProductPlanning.PPC_planning_calendar__c ;
                    productPlanning.Procurement_Type__c = stageProductPlanning.Procurement_Type__c ;
                    productPlanning.Product_Planning_External_Id__c = stageProductPlanning.Staging_Product_Planning_External_Id__c;
                    productPlanning.Profit_Center__c = stageProductPlanning.Profit_Center__c ;
                    productPlanning.Purchasing_Group__c = stageProductPlanning.Purchasing_Group__c ;
                    productPlanning.Safety_Stock__c = Decimal.valueOf(stageProductPlanning.Safety_Stock__c );
                    productPlanning.Safety_time_in_workdays__c = Decimal.valueOf(stageProductPlanning.Safety_time_in_workdays__c );
                    productPlanning.Service_level__c = Decimal.valueOf(stageProductPlanning.Service_level__c );
                    productPlanning.Special_procurement_type__c = stageProductPlanning.Special_procurement_type__c;
                    productPlanning.Plant_Specific_Material_Status__c = stageProductPlanning.Plant_Specific_Material_Status__c;
                    
                    if(stageProductPlanning.Plant_Spfc_Material_Status_Vldty_Date__c!=null && stageProductPlanning.Plant_Spfc_Material_Status_Vldty_Date__c!='') {
                        if(stageProductPlanning.Plant_Spfc_Material_Status_Vldty_Date__c.length() == 8) {
                            String month=stageProductPlanning.Plant_Spfc_Material_Status_Vldty_Date__c.substring(4,6);
                            String day=stageProductPlanning.Plant_Spfc_Material_Status_Vldty_Date__c.substring(6,8);
                            String year=stageProductPlanning.Plant_Spfc_Material_Status_Vldty_Date__c.substring(0,4);
                            productPlanning.Plant_Spfc_Material_Status_Vldty_Date__c = Date.valueOf(year+'-'+month+'-'+day);
                        } else {
                            flag=false;
                            errorMessage=errorMessage+' Incorrect Date from which the plant-specific material status is valid';
                        }
                    }
                    
                    if(stageProductPlanning.Material_Number__c != null ) {
                        if(productMap.get(stageProductPlanning.Material_Number__c) != null) {
                            productPlanning.Material_Number__c = productMap.get(stageProductPlanning.Material_Number__c);
                        }else{
                            flag=false;
                            errorMessage=errorMessage+'Incorrect sales organization code';
                        }
                    }
                    
                    if(stageProductPlanning.Plant__c != null) {
                        productPlanning.Plant__c = plantMap.get(stageProductPlanning.Plant__c ); 
                    }
                    
                }
                else {
                    flag=false;
                    errorMessage = errorMessage+'Product Planning External Id not found';
                }
                    
                if(flag) {
                    stageProductPlanning.Is_Processed__c = true;
                    productPlanningList.add(productPlanning);
                }else {
                    stageProductPlanning.Is_Error__c = true;
                    stageProductPlanning.Error_Description__c = errorMessage;
                }
                stageProductPlanningList.add(stageProductPlanning);
                    
            }
            
            List<Database.UpsertResult> upsertResults = Database.upsert(productPlanningList,Product_Planning__c.Product_Planning_External_Id__c);// upsert productList material_Number__c;
            List<Database.SaveResult> updateResults = Database.update(stageProductPlanningList);//update stageProductList;
            
        }
        catch(Exception excptn) {
                  
        }
    }
    
    global void finish(Database.BatchableContext BC) {
    }
    
    
}
public class AccountSearchController {

    public List<Account> accountList { get; set; }
    public Boolean showAccounts{get;set;}

    public String accountName{get;set;}

    public AccountSearchController(){
    
    accountList=new List<Account>();
    showAccounts=false;
    
    }
    public PageReference searchAccount() {
    showAccounts=true;
    String query='select name from Account where Name like \'%'+String.escapeSingleQuotes(accountName)+'%\'';
    accountList =Database.query(query);
   // System.assertEquals(accountList,null);
        return null;
    }

}
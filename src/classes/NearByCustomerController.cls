/****
    Developer : Anjith kumar
    Class Name: NearByCustomerController
    Version: 1.0
    Description: This class is to get the all accounts with location whose location is near by distance given in custom settings
    Usage: This is used in NearByCustomer and NearByCustomer_V2
    
****/
public class NearByCustomerController {
    
    public List<List<String>> getMapData() {
        //system.assertEquals(dataForMap,null);
        return dataForMap ;
    }
    
    public   Account currentAccount{get;set;}
    public Boolean renderMap{get;set;}
    
    List<List<String>> dataForMap = new List<List<String>>();
    public List<List<String>> allNearbyStores {get;set;} 
    public NearByCustomerController(ApexPages.StandardController stdController){
        allNearbyStores = new List<List<String>>();
        Account acc=(Account)stdController.getRecord();
        //System.AssertEquals(acc,null);
        try{
            currentAccount=[select id,Name, Location__Latitude__s,Location__Longitude__s from Account where Id=:acc.id AND Location__Latitude__s!=null];
            renderMap = true;
            Decimal lat = currentAccount.Location__Latitude__s;
            Decimal longVal = currentAccount.Location__Longitude__s;
            List<NearBySettings__c> settingsList=[select Name,Units__c,Distance__c from NearBySettings__c where Is_Active__C=true];
            Decimal d=settingsList[0].Distance__c;
            String units=settingsList[0].units__c;
            String q = 'SELECT Id,Name,ownerId,Customer_Group_Primary__c,BillingStreet,BillingCity,BillingState,BillingPostalCode,BillingCountry,Location__Latitude__s,Location__Longitude__s  FROM Account where id!=\'' +currentAccount.Id +'\' and Location__Latitude__s!=null and Location__Longitude__s!=null and DISTANCE(Location__c, GEOLOCATION( ' + String.valueOf(lat) + ',' + String.valueOf(longVal)+ '), \''+units+'\') < '+d;
            for(Account nearByCustomer : database.query(q)){
                String Name=nearByCustomer.Name.replace('\'',' ');
                //System.assertEquals(name,null);
                String address='';
                if(nearByCustomer.billingStreet!=null && nearByCustomer.BillingStreet!='')
                    address=nearByCustomer.billingStreet;
                if(nearByCustomer.BillingCity!=null && nearByCustomer.BillingCity!='')
                    address=address+', '+nearByCustomer.BillingCity;
                if(nearByCustomer.BillingState!=null && nearByCustomer.BillingState!='')
                    address=address+', '+nearByCustomer.BillingState;
                if(nearByCustomer.BillingCountry!=null && nearByCustomer.BillingCountry!='')
                    address=address+', '+nearByCustomer.BillingCountry;
                if(nearByCustomer.BillingPostalCode!=null && nearByCustomer.BillingPostalCode!='')
                    address=address+', '+nearByCustomer.BillingPostalCode;
                String specialChars = '&|^|@|\''; 
                for(integer i=0; i<specialChars.split('|').size(); i++)
                address = address.replace(specialChars.split('|')[i], '');
              //  System.assertEquals(address,null);
                String fullURL =URL.getSalesforceBaseUrl().toExternalForm() +  '/' +  nearByCustomer.Id;
                if(nearByCustomer.OwnerId==userInfo.getUserId()){
                dataForMap.add(new List<String>{'\'' +fullURL+'\'' ,'\'' +Name+'\'' ,'\'' +nearByCustomer.Location__Latitude__s +'\'' ,'\'' +nearByCustomer.Location__Longitude__s+'\'' ,'\'' + nearByCustomer.Id+'\'' ,'\'' + nearbycustomer.Customer_Group_Primary__c+'\'' ,'\''+address +'\''}); 
                }
                allNearbyStores.add(new List<String>{'\'' +fullURL+'\'' ,'\'' +Name+'\'' ,'\'' +nearByCustomer.Location__Latitude__s +'\'' ,'\'' +nearByCustomer.Location__Longitude__s+'\'' ,'\'' + nearByCustomer.Id+'\'' ,'\'' + nearbycustomer.Customer_Group_Primary__c+'\'' ,'\''+address +'\''}); 
            }
        }
        catch(Exception e)
        {
            renderMap = false;
            System.assertEquals(e.getMessage(),null);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Address of current account not set properly'));
        }
    }
    
}
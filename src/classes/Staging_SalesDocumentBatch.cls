global class Staging_SalesDocumentBatch implements Database.Batchable<sObject> {
    String query;
    global Staging_SalesDocumentBatch (){
        query='select id,';
        List<Schema.FieldSetMember> fieldSetMemberList =Schema.SObjectType.Staging_Sales_Document__c.fieldSets.getMap().get('All_Fields').getFields();
        
        for(Schema.FieldSetMember field : fieldSetMemberList)
        {
            query=query+field .getFieldPath()+',';             
        }
        query=query.substring(0,query.length()-1)+' from Staging_Sales_Document__c where is_Processed__c=false';
        System.debug(query);
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        try{
            List<Staging_Sales_Document__c> stageSalesDocList=new List<Staging_Sales_Document__c>();
            List<Opportunity> salesDocList = new List<Opportunity>();
            Set<String> customerListExtIdsSet= new set<String>();
            Set<String> salesDocTypeExtIdsSet=new Set<String>();
            Set<String> salesOrderReasonExtIdsSet=new Set<String>();
            Map<String,Id> salesDocTypeMap = new Map<String,Id>();
            Map<String,Id> salesOrderReasonMap = new Map<String,Id>();
            Map<String,Id> customerMap = new Map<String,Id>();
            for(sObject sDoc : scope) { 
                 Staging_Sales_Document__c stageSaleDoc=(Staging_Sales_Document__c)sDoc;
                 customerListExtIdsSet.add(stageSaleDoc.Customer_Number__c );
                 salesDocTypeExtIdsSet.add(stageSaleDoc.Sales_Document_Type__c);
                 salesOrderReasonExtIdsSet.add(stageSaleDoc.Order_Reason__c);
            }
            for(Account customer : [select id,Customer_Number_1__c from Account where Customer_Number_1__c in:customerListExtIdsSet]) {
            	customerMap.put(customer.Customer_Number_1__c,customer.Id);
            }
            for(Sales_Document_Type__c salesDocType : [select id,Sales_Document_Type_Code__c from Sales_Document_Type__c where Sales_Document_Type_Code__c in:salesDocTypeExtIdsSet ]) {
            	salesDocTypeMap.put(salesDocType.Sales_Document_Type_Code__c,salesDocType.id);
            }
            for(Sales_Order_Reason__c salesOrderReason : [select id,Sales_Order_Reason_Code__c from Sales_Order_Reason__c where Sales_Order_Reason_Code__c in:salesOrderReasonExtIdsSet]) {
            	salesOrderReasonMap.put(salesOrderReason.sales_order_reason_code__c,salesOrderReason.id);
            } 
           for(sObject sDoc : scope) { 
               Staging_Sales_Document__c stageSaleDoc=(Staging_Sales_Document__c)sDoc;
			   Opportunity salesDoc = new Opportunity();
               Boolean flag=true;
               String errorMessage='';
               if(customerMap.get(stageSaleDoc.Customer_Number__c)!=null){
               		salesDoc.Name=customerMap.get(stageSaleDoc.Customer_Number__c);
                   	salesDoc.CloseDate=System.today();//Need to be change TBD
                   salesDoc.StageName='Closed Won';
                   salesDoc.AccountId=customerMap.get(stageSaleDoc.Customer_Number__c);
                   salesDoc.Sold_to_party__c=customerMap.get(stageSaleDoc.Sold_to_party__c);
                   salesDoc.Customer_Number__c=stageSaleDoc.Customer_Number__c;
                   salesDoc.Sales_Document__c=stageSaleDoc.Sales_Document__c;
                   salesDoc.Sales_Document_Type__c=salesDocTypeMap.get(stageSaleDoc.Sales_Document_Type__c);
                   salesDoc.Sales_Order_Reason__c=salesOrderReasonMap.get(stageSaleDoc.Sales_Organization__c);
                   salesDoc.Exchange_Rate_for_Price_Determination__c=Integer.valueOf(stageSaleDoc.Exchange_Rate_for_Price_Determination__c);
                   salesDoc.Customer_purchase_order_number__c=stageSaleDoc.Customer_purchase_order_number__c;
                    if(stageSaleDoc.Customer_purchase_order_date__c.length()==8) {
                        String month=stageSaleDoc.Customer_purchase_order_date__c.substring(4,6);
                        String day=stageSaleDoc.Customer_purchase_order_date__c.substring(6,8);
                        String year=stageSaleDoc.Customer_purchase_order_date__c.substring(0,4);
                        salesDoc.Customer_purchase_order_date__c=Date.valueOf(year+'-'+month+'-'+day);
                    } else {
                        flag=false;
                        errorMessage=errorMessage+' \n Incorrect customer purchase order date';
                    }
                   
                   
               }
               else{
               		flag=false;
                    errorMessage='Incorrect Customer Number';
               }
               if(flag) {
               		salesDocList.add(salesDoc);
                    stageSaleDoc.is_Processed__c=true;
               }
               else{
               		stageSaleDoc.is_error__c=true;
                   stageSaleDoc.Error_Discription__c=errormessage;
               }
               stageSalesDocList.add(stageSaleDoc);
           }
            upsert salesDocList Sales_Document__c;
            update stageSalesDoclist;
            System.debug(salesDocList);
            System.debug(stageSalesDoclist);
        }catch(Exception e){
            System.debug(e.getMessage());
        }
    }
    
    global void finish(Database.BatchableContext BC){
    }
}
/******
        Developer Name: Anjith kumar
        Class Name: LaunchChecklistController
        Description: This is used for save and submit checklist response of a checklist realted to account
        Usage: This class used in LaunchChecklistV2
*****/
public class LaunchChecklistController {

    public LaunchChecklistController(ApexPages.StandardController controller) {
             try{
        isSubmitted=false;
        showSuccessOnSave =false;
        showSuccessOnSaveNSubmit=false;
        hasChecklistAssigned=true;
        allQuestions =new List<QuestionsWrapperClass > ();
        accountId=((Account)controller.getRecord()).Id;//Apexpages.currentPage().getParameters().get('accountId');
        checklistAssignmentList=new List<SurveyTaker__c>();
        Set<Id> surveyIdsSet= new Set<Id>();
        checklistAssignmentList = [select Id,Survey__r.Name,Survey__c,Is_Active__c,Is_Submitted__c from SurveyTaker__c  where Account__c =: accountId and (Is_Active__c = true or Is_Submitted__c=true)];
        for(SurveyTaker__c checklistAssignment : checklistAssignmentList ) {
            if(checklistAssignment.Is_Submitted__c){
                isSubmitted=true;
            }
            else{
                surveyAndAssignmentIdsMap.put(checklistAssignment.Survey__c,checklistAssignment.Id);
            }
        }
        
        if(!isSubmitted && checklistAssignmentList.size()==0){
           hasChecklistAssigned=false; 
        }
        if(isSubmitted==true || hasChecklistAssigned ==false){
            showInputPanel =false;
        }
        else{
            showInputPanel = true;
        }
        Integer orderNo=1;
        for(Survey_Question__c question:[Select Type__c, Id, Survey__c,Required__c, Question__c,OrderNumber__c,Name, Choices__c From Survey_Question__c WHERE Survey__c in : surveyAndAssignmentIdsMap.keySet()]) {
            questionIdsSet.add(question.id);
            allQuestions.add(new QuestionsWrapperClass(question,orderNo));
            orderNo++;
        }
       // System.assertEquals(null,allQuestions);
         for(SurveyQuestionResponse__c  response : [select Id, Survey_Question__c ,SurveyTaker__c from SurveyQuestionResponse__c  where Survey_Question__c in:questionIdsSet and SurveyTaker__c in: surveyAndAssignmentIdsMap.values() ]) {
          questionsAndResponsesMap.put(response.Survey_Question__c ,response.Id);
         
         }
        }catch(Exception e){
            hasChecklistAssigned=false;
        }
    }

    public Id accountId{get;set;}
    public Boolean hasChecklistAssigned{ get; set;}
    public Boolean isSubmitted{ get; set;}
    List<SurveyTaker__c> checklistAssignmentList;
    public List<QuestionsWrapperClass > allQuestions {get; set;} 
    Map<Id,Id> surveyAndAssignmentIdsMap= new Map<Id,Id>();
    Set<Id> questionIdsSet=new Set<Id>();
    Map<Id,Id> questionsAndResponsesMap = new Map<Id,Id>();
    public Boolean showInputPanel {get; set;}
    public Boolean showSuccessOnSave { get; set;}
    public Boolean showSuccessOnSaveNSubmit { get; set;}
    public LaunchChecklistController () {
       
        //System.assertEquals(checklistAssignmentList ,null);
    }
    public class QuestionsWrapperClass {
          public Id questionId {get; set;}
          public Id surveyId {get; set;}
          public String question{ get; set;}
          public Integer orderNumber { get; set;}
          public String   selectedOption {get;set;}
          public List<String> selectedOptions {get;set;}
          public List<SelectOption> singleOptions{get; set;}
          public List<SelectOption> multiOptions {get; set;}
          public String freeText { get;  set;}
          public Boolean  required {get; set;}
          public String   renderFreeText             {get; set;}
          public String   renderSelectRadio      {get; set;}
          public String   renderSelectCheckboxes {get; set;} 
          public QuestionsWrapperClass(Survey_Question__c sq,Integer orderNo) {
              questionId=sq.id;
              surveyId =sq.Survey__c;
              question = sq.Question__c;
              orderNumber = orderNo;
              required = sq.Required__c;
              selectedOption = '';
              freeText='';
              selectedOptions = new List<String>();
              if (sq.Type__c=='Single Select'){
                renderSelectRadio='true';
                singleOptions = stringToSelectOptions(sq.Choices__c);            
                renderSelectCheckboxes='false';
                renderFreeText='false';                
                                               
              }
              else if (sq.Type__c=='Multi Select') { 
                     
                renderSelectCheckboxes='true';
                multiOptions = stringToSelectOptions(sq.Choices__c);
                renderSelectRadio='false';
                renderFreeText='false';
            }
            else if (sq.Type__c=='Free Text'){
                renderFreeText='true';
                renderSelectRadio='false';
                renderSelectCheckboxes='false';
           }
              
          }
          
          /** Splits up the string as given by the user and adds each option
        *  to a list to be displayed as option on the Visualforce page
        *  param: str   String as submitted by the user
        *  returns the List of SelectOption for the visualforce page
        */  
      private List<SelectOption> stringToSelectOptions(String str){
        if (str == '')
        {
            return new List<SelectOption>();
        }
        List<String> strList = str.split('\n');
      
        List<SelectOption> returnVal = new List<SelectOption>();
        Integer i = 0;
        for(String s: strList){
            if (s!='') {    
                if (s != 'null' && s!= null) {
                    String sBis = s.replace(' ', '%20');
                    
                    
                    /*RSC2012-02-20
                    String st = s.replace (' ', '&nbsp;');
                    returnVal.add(new SelectOption(String.valueOf(i),st));
                    */
                    returnVal.add(new SelectOption(String.valueOf(i),s));
                    System.debug('*****VALUES: ' + s);
                    i++;
                }
            }
        }
        
        return returnVal;
      } 
   
    
    }
    public pageReference saveResults(){
        //System.assertEquals(null,allQuestions);
        List <SurveyQuestionResponse__c> sqrList = new List<SurveyQuestionResponse__c>();
       
         for (QuestionsWrapperClass q : allQuestions)
        {
            SurveyQuestionResponse__c sqr = new SurveyQuestionResponse__c();
            if (q.renderSelectRadio == 'true')
            {
                
                if (q.required &&  (q.selectedOption == null || q.selectedOption == ''))
                {
                    Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please fill out all required fields'));
                    return null ;
                }
                
                if (q.selectedOption == null || q.selectedOption == '')
                {
                    sqr.Response__c = '';
                }
                else
                {
                    sqr.Response__c = q.singleOptions.get(Integer.valueOf(q.selectedOption)).getLabel();
                }
                
                
            }
            else if (q.renderFreeText == 'true')
            {
                if (q.required && q.freeText == '')
                {
                    Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please fill out all required fields'));
                    return null ;
                }
                System.debug('*****Select Radio ' + q.freeText);
                
                sqr.Response__c = q.freeText;
                
            }
            else if (q.renderSelectCheckboxes == 'true')
            {
                if (q.required && (q.selectedOptions == null || q.selectedOptions.size() == 0))
                {
                    Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please fill out all required fields'));
                    return null ;
                }
                
                for (String opt : q.selectedOptions)
                {
                    sqr = new SurveyQuestionResponse__c();
                    if (opt == '' || opt == null)
                    {
                        sqr.Response__c = '';
                    }               
                    else
                    {   
                        sqr.Response__c = q.multiOptions.get(Integer.valueOf(opt)).getLabel();
                    }
                   
                }
            }
            sqr.Survey_Question__c = q.questionId;
            sqr.SurveyTaker__c = surveyAndAssignmentIdsMap.get(q.surveyId);
            sqr.Id=questionsAndResponsesMap.get(q.questionId);
            sqrList.add(sqr);
        }
          showSuccessOnSave=true;
         showInputPanel=false;
       database.upsert(sqrList);
      
       //System.assertEquals(sqrList,null);
       return null;
    }
     public void saveAndSubmitResults(){ 
         saveResults();
         Set<SurveyTaker__c> assignmentSet=new Set<SurveyTaker__c>();
         for(Id assignmentId : surveyAndAssignmentIdsMap.values()){
             assignmentSet.add(new SurveyTaker__c(Id=assignmentId,is_Submitted__c=true,is_Active__c=false));
         }
        List<SurveyTaker__c> assignmentList= new List<SurveyTaker__c>();
        assignmentList.addAll(assignmentSet);
         database.update(assignmentList);
         showSuccessOnSave=false;
         showSuccessOnSaveNSubmit= true;
         showInputPanel=false;
     }  
}
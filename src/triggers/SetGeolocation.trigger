/***
    Developer: Anjith kumar
    class Name: SetGeolocation
    Version:1.0
    Date:16/01/2014
**/
// Trigger runs getLocation on Accounts with no Geolocation
trigger SetGeolocation on Account (after insert, after update) {
    for (Account a : trigger.new)
        if (a.Location__Latitude__s == null)
            LocationCallouts.getLocation(a.id);
}